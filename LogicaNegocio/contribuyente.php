<?php
include_once ('../../AccesoDatos/contribuyente.php');
include_once ('../../Entidades/contribuyente.php');


class LogicaNegocioContribuyentes
{

    public function getUsuarios()
    {
        return AccesoDatosContribuyentes::getUsuarios();
    }

    public function crearUsuario($folio, $nombre, $apellidoPaterno, $apellidoMaterno, $fraccion, $estado, $id)
    {
        $entidad = new EntidadContribuyentes();

        $entidad->setFolio($folio);
        $entidad->setNombre($nombre);
        $entidad->setApellidoPaterno($apellidoPaterno);
        $entidad->setApellidoMaterno($apellidoMaterno);
        $entidad->setFraccion($fraccion);
        $entidad->setEstado($estado);
        if (!is_null($id)) {
            $entidad->setId($id);

        }

        return AccesoDatosContribuyentes::crearUsuario($entidad);
    }

    public function getUsuarioPorId($id)
    {
        return AccesoDatosContribuyentes::getUsuarioPorId($id);
    }

    public function eliminarUsuario($id)
    {
        return AccesoDatosContribuyentes::eliminarUsuario($id);
    }
    public function validar_campo($campo)
    {
        $campo = trim($campo);
        $campo = stripcslashes($campo);
        $campo = htmlspecialchars($campo);

        return $campo;
    }

    public function EsFolioValido($campo){
        $this->validar_campo($campo);

    }

    public function EsNombreValido(){

    }

    public function EsApellidoPaternoValido(){

    }

    public function EsApellidoMaternoValido(){

    }

    public function EsFraccionValido(){

    }

    public function EsEstadoValido(){

    }
   /* public function getPrivilegio($p)
    {
        $privilegio = "";
        switch ($p) {
            case 1:
                $privilegio = "Administrador";
                break;

            case 2:
                $privilegio = "Usuario";
                break;

            default:
                $privilegio = "- No definido -";
                break;
        }

        return $privilegio;
    } */

}
