<?php
include 'Entidades/ufinal.php';
include 'AccesoDatos/ufinal.php';

class UsuarioControlador
{

    public static function login($usuario, $contrasena)
    {
        $obj_usuario = new Usuario();
        $obj_usuario->setUsuario($usuario);
        $obj_usuario->setContrasena($contrasena);

        return UsuarioDao::login($obj_usuario);
    }

    public function getUsuario($usuario, $contrasena)
    {
        $obj_usuario = new Usuario();
        $obj_usuario->setUsuario($usuario);
        $obj_usuario->setContrasena($contrasena);

        return UsuarioDao::getUsuario($obj_usuario);
    }

    public function registrar($usuario, $contrasena, $idtipousuario)
    {
        $obj_usuario = new Usuario();
        $obj_usuario->setUsuario($usuario);
        $obj_usuario->setIdTipoUsuario($idtipousuario);
        $obj_usuario->setContrasena($contrasena);

        return UsuarioDao::registrar($obj_usuario);
    }
    public function validar_campo($campo)
    {
        $campo = trim($campo);
        $campo = stripcslashes($campo);
        $campo = htmlspecialchars($campo);

        return $campo;
    }

}
