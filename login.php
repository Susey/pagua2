<?php include 'partials/head.php';?>
<?php include 'partials/header.php';?>
<div class="container">

	<div class="starter-template">

		<div class="row">
			<div class="col-md-4 col-md-offset-4 p-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<form id="loginForm" action="validarCode.php" method="POST" role="form">
							<h2>Inicie sesión aquí</h2>

							<div class="form-group">
								<label for="usuario">Usuario</label>
								<input type="text" name="txtUsuario" class="form-control" id="usuario" autofocus required placeholder="Usuario">
							</div>

							<div class="form-group">
								<label for="password">Contraseña</label>
								<input type="password" name="txtPassword" class="form-control" required id="password" placeholder="********">
							</div>

							<button type="submit" class="btn btn-success">Ingresar</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<?php include 'partials/footer.php';?>