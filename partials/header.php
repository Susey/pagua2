<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex">
        <!--<img src ="assets/img/icons/pagualog.png" width="120", height="60"  >-->
        <div class="logo mr-auto">
            <h1 class="text-light"><a href="index.php"><span>Pagos del servicio de agua</span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.php"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <?php

        if (isset($_SESSION["usuario"])){
            ?>
            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="drop-down"><a href="#">Registro</a>
                        <ul>
                            <li><a href="Usuarios.php">Usuarios</a></li>
                            <li><a href="Contribuyentes.php">Contribuyentes</a></li>
                        </ul>
                    <li><a href="Consulta.php">Consulta</a></li>
                    <li><a href="Pagos.php">Pagos</a></li>
                    <li class="drop-down"><a href="#"><?php echo $_SESSION["usuario"]["usuario"];?></a>
                        <ul>
                            <li><a>Usted es <?php echo $_SESSION["usuario"]["idTipoUsuario"] == 1 ? 'Admin' : 'Operador'; ?></a></li>
                            <li><a href="../cerrar-sesion.php">Cerrar Sesion</a></li>
                        </ul>

                    <!--<li class="drop-down"><a href="#">Informes</a>
                      <ul>
                      <li><a href="Entrada.php">Entrada de capital</a></li>
                      <li><a href="Deudores.php">Deudores</a></li>-->
                </ul>
            </nav><!-- .nav-menu -->
            <?php
        }

        ?>
    </div>
</header><!-- End Header -->