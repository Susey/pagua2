<!DOCTYPE html>
<html lang="es">
  <head>
    <?php session_start();?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>PAGUA</title>

      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

      <!-- Vendor CSS Files -->
      <link href="/pagua2/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/pagua2/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
      <link href="/pagua2/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
      <link href="/pagua2/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
      <link href="/pagua2/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="/pagua2/assets/vendor/venobox/venobox.css" rel="stylesheet">

      <!-- Template Main CSS File -->
      <link href="/pagua2/assets/css/style.css" rel="stylesheet">



  </head>

  <body>