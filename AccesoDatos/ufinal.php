<?php

include_once 'conexion.php';
include_once 'Entidades/ufinal.php';

class UsuarioDao extends Conexion
{
    protected static $cnx;

    private static function getConexion()
    {
        self::$cnx = Conexion::conectar();
    }

    private static function desconectar()
    {
        self::$cnx = null;
    }

    /**
     * Metodo que sirve para validar el login
     *
     * @param      object         $usuario
     * @return     boolean
     */
    public static function login($usuario)
    {
        $query = "SELECT * FROM UFinal WHERE usuario = :usuario AND contrasena = :contrasena";

        self::getConexion();

        $resultado = self::$cnx->prepare($query);

        $usu = $usuario->getUsuario();
        $pas = $usuario->getContrasena();

        $resultado->bindParam(":usuario", $usu);
        $resultado->bindParam(":contrasena", $pas);

        $resultado->execute();

        if ($resultado->rowCount() > 0) {
            $filas = $resultado->fetch();
            if ($filas["usuario"] == $usuario->getUsuario()
                && $filas["contrasena"] == $usuario->getContrasena()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Metodo que sirve obtener un usuario
     *
     * @param      object         $usuario
     * @return     object
     */
    public static function getUsuario($usuario)
    {
        $query = "SELECT id,usuario,idTipoUsuario FROM UFinal WHERE usuario = :usuario AND contrasena = :contrasena";

        self::getConexion();

        $resultado = self::$cnx->prepare($query);

        $usu = $usuario->getUsuario();
        $pas = $usuario->getContrasena();

        $resultado->bindParam(":usuario", $usu);
        $resultado->bindParam(":contrasena", $pas);

        $resultado->execute();

        $filas = $resultado->fetch();

        $usuario = new Usuario();
        $usuario->setId($filas["id"]);
        $usuario->setUsuario($filas["usuario"]);
        $usuario->setIdTipoUsuario($filas["idTipoUsuario"]);

        return $usuario;
    }

    /**
     * Metodo que sirve para registrar usuarios
     *
     * @param      object         $usuario
     * @return     boolean
     */
    public static function registrar($usuario)
    {
        $query = "INSERT INTO UFinal (usuario,contrasena,idTipoUsuario) VALUES (:usuario,:contrasena,:idTipoUsuario)";

        self::getConexion();

        $resultado = self::$cnx->prepare($query);

        $resultado->bindValue(":usuario", $usuario->getUsuario());
        $resultado->bindValue(":contrasena", $usuario->getContrasena());
        $resultado->bindValue(":idTipoUsuario", $usuario->getIdTipoUsuario());

        if ($resultado->execute()) {
            return true;
        }

        return false;
    }
}
