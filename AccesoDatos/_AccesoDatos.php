<?php
    $contibuyente = new Contibuyentes();

     abstract  class _AccesoDatos 
    {
        # Función para guardar datos
        abstract public function Guardar();
        # Funcion para insertar datos
        abstract public function GenerarCodigo();
        # Función para eliminar datos
        abstract public function Eliminar();
        # Función para mostrar datos
        abstract public function ObtenerVista();
    }
?>