<?php

include_once '../../conexion.php';
include_once '../../Entidades/contribuyente.php';

class AccesoDatosContribuyentes extends Conexion
{
    protected static $conexion;

    private static function getConexion()
    {
        self::$conexion = Conexion::conectar();
    }

    private static function desconectar()
    {
        self::$conexion = null;
    }

    /**
     * Metodo que sirve para obtener o buscar un usuario por su id
     *
     * @param      object         $usuario
     * @return     object
     */
    public static function getUsuarioPorId($id)
    {
        $query = "SELECT id,folio,nombre,apellidoPaterno,apellidoMaterno,fraccion,estado FROM contribuyente WHERE id = :id";

        self::getConexion();

        $resultado = self::$conexion->prepare($query);

        $resultado->bindParam(":id", $id);

        $resultado->execute();

        $filas = $resultado->fetch();

        $entidad = new EntidadContribuyentes();
        $entidad->setId($filas["id"]);
        $entidad->setFolio($filas["folio"]);
        $entidad->setNombre($filas["nombre"]);
        $entidad->setApellidoPaterno($filas["apellidoPaterno"]);
        $entidad->setApellidoMaterno($filas["apellidoMaterno"]);
        $entidad->setFraccion($filas["fraccion"]);
        $entidad->setEstado($filas["estado"]);

        return $entidad;
    }

    /**
     * Metodo que sirve para eliminar un usuario
     *
     * @param      object         $usuario
     * @return     boolean
     */
    public static function eliminarUsuario($id)
    {
        $query = "DELETE FROM contribuyente WHERE id = :id";

        self::getConexion();

        $resultado = self::$conexion->prepare($query);

        $resultado->bindParam(":id", $id);

        $resultado->execute();

        if ($resultado->execute()) {
            return true;
        }

        return false;
    }

    /**
     * Metodo que sirve obtener o listar todos los usuarios
     *
     * @return     object
     */
    public static function getUsuarios()
    {
        $query = "SELECT id,folio,nombre,apellidoPaterno,apellidoMaterno,fraccion,estado FROM contribuyente";

        self::getConexion();

        $resultado = self::$conexion->prepare($query);

        $resultado->execute();

        $filas = $resultado->fetchAll();

        return $filas;
    }

    /**
     * Metodo que sirve para crear y editar usuarios
     *
     * @param      object         $usuario
     * @return     boolean
     */
    public static function crearUsuario($entidad)
    {
        if (is_null($entidad->getId())) {
            $query = "INSERT INTO contribuyente (folio,nombre,apellidoPaterno,apellidoMaterno,fraccion,estado) VALUES (:folio,:nombre,:apellidoPaterno,:apellidoMaterno,:fraccion,:estado)";
        } else {
            $query = "UPDATE contribuyente SET folio=:folio,nombre=:nombre,apellidoPaterno=:apellidoPaterno,apellidoMaterno=:apellidoMaterno,fraccion=:fraccion,estado=:estado WHERE id=:id";
        }

        self::getConexion();

        $resultado = self::$conexion->prepare($query);

        $folio = $entidad->getFolio();
        $nombre = $entidad->getNombre();
        $apellidoPaterno = $entidad->getApellidoPaterno();
        $apellidoMaterno = $entidad->getApellidoMaterno();
        $fraccion = $entidad->getFraccion();
        $estado = $entidad->getEstado();

        if (!is_null($entidad->getId())) {
            $id = $entidad->getId();
            $resultado->bindParam(":id", $id);
         } # else {
            # $resultado->bindParam(":privilegio", $privilegio);
        # }
        $resultado->bindParam(":folio", $folio);
        $resultado->bindParam(":nombre", $nombre);
        $resultado->bindParam(":apellidoPaterno", $apellidoPaterno);
        $resultado->bindParam(":apellidoMaterno", $apellidoMaterno);
        $resultado->bindParam(":fraccion", $fraccion);
        $resultado->bindParam(":estado", $estado);

        if ($resultado->execute()) {
            return true;
        }

        return false;
    }

}