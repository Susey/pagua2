<?php include ('../partials/head.php'); ?>

<?php

if (isset($_SESSION["usuario"])) {
# if ($_SESSION["usuario"]["idTipoUsuario"] == 2) {
#     header("location:usuario.php");
# }
?>

<?php include_once ('../partials/header.php');?>

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Inicio</a></li>
          <li>Registro</li>
        </ol>
        <h2>Contribuyentes</h2>
      </div>
    </section><!-- End Breadcrumbs -->


 <!-- ======= Contribuyentes Selecction ======= -->
 <section id="about" class="about">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="../assets/img/icons/adduser.png" class="img-fluid" alt="" >
      </div>
      <div class="card-body">
        <h4>Registrar Contribuyente</h4>
        <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Buscar contribuyente:<input type="search" 
        class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0"></label>
        <button class="btn btn-primary" type="submit">Buscar</button>
        <div class="form-group">
          <div class="col-sm-4 col-lg-3 mb-3 mb-sm-0">
          <input type="checkbox" class="custom-control-input">
          <label for="inputText3" class="col-form-label" disabled="">Folio</label>
          <input id="inputText3" type="text" class="form-control" disabled="">
        </div>
      </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label">Nombre(s)</label>
          <input id="inputText3" type="text" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label">Apellido Paterno</label>
          <input id="inputText3" type="text" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label">Apellido Materno</label>
          <input id="inputText3" type="text" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label">Seccion de la comunidad</label>
          <select name="select">
            <option value="value1">Arriba</option> 
            <option value="value3">Abajo</option>
          </select>
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label">Estado de contribuyente</label>
          <select name="select">
            <option value="value1">ACTIVO</option> 
            <option value="value3">INACTIVO</option>
          </select>
        </div> 
        <div class="form-group">
          <label for="inputText3" class="col-form-label">Cantidad de tomas</label>
          <select name="select">
            <option value="value1">0</option> 
            <option selected value="value2">1</option> 
            <option value="value3">2</option>
            <option value="value4">3</option>
            <option value="value5">4</option>
            <option value="value6">5</option>
            <option value="value7">6</option>
            <option value="value8">7</option>
            <option value="value9">8</option>
            <option value="value10">9</option>
            <option value="value11">10</option>
          </select>
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label">Cantidad de ganado</label>
          <!--div class="col-12 col-sm-8 col-lg-6">-->
              <input required="" type="number" min="0" max="100" placeholder="0 - 100" class="form-control">
          <!--</div>-->
        </div>
      </div>
          <a href="#" class="btn btn-rounded btn-success">Guardar</a>
          <a href="#" class="btn btn-rounded btn-danger">Cancelar</a>
      </div>
    </div>
  </div>
</div>
 </section>
     <?php
     } else {
         header("location: ../login.php");
     }

     include ('../partials/footer.php');
     ?>

