<?php include ('../partials/head.php'); ?>

<?php

if (isset($_SESSION["usuario"])) {

?>

<?php include_once ('../partials/header.php');?>

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Inicio</a></li>
          <li>Consulta</li>
        </ol>
        <h2>Consultas</h2>
      </div>
    </section><!-- End Breadcrumbs -->
    
        <!-- ======= Team Section ======= -->
        <section id="team" class="team">
          <div class="container">
    
            <div class="row">
              <!-- ============================================================== -->
              <!-- data table  -->
              <!-- ============================================================== -->
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                
                <div class="card">
                
                  <div class="card-body">
                    <div class="table-responsive">
                      <div class="col-xl-12">
                        <div class="row">
                          
                        <div class="form-group">
                          <div style="color:rgb(116, 111, 111)";>De:
                          <input type="date" id="date-mask" im-insert="true"></div>
                        </div>
                        <div class="form-group">
                          <div style="color:rgb(116, 111, 111)";>A:
                          <input type="date" id="date-mask" im-insert="true"></div>
                        </div>
                        <div class="form-group">
                          <div style="color:rgb(116, 111, 111)";>Sección
                          <select name="select">
                            <option value="value1">TODOS</option> 
                            <option value="value2">ABAJO</option>
                            <option value="value3">ARRIBA</option>
                          </select></div>
                        </div>
                        <div class="form-group">
                          <div style="color:rgb(116, 111, 111)";>Deudas
                          <select name="select">
                            <option value="value1">TODOS</option>
                            <option value="value1">PAGADO</option> 
                            <option value="value3">NO PAGADO</option>
                          </select></div>
                        </div>
                      </div>
                          <div class="col-sm-12 col-md-6">
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                          <p style="color:rgb(116, 111, 111)";>Buscar:<input type="search" 
                            class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0">
                            <button class="btn btn-primary" type="submit">Buscar</button>
                        </div>
                      </div>
                        </div>
                      </div>
                        <div class="row">
                          <div class="table-responsive">
                          <div class="col-sm-12"><table id="example" class="table table-striped table-bordered second dataTable" style="width: 100%;"
                            role="grid" aria-describedby="example_info">
                            <thead>
                            <tr role="row">
                              <th class="sorting_asc" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-sort="ascending"
                                  aria-label="Name: activate to sort column descending"
                                  style="width: 73px;">Folio</th>
                              <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                  colspan="1" aria-label="Position: activate to sort column ascending"
                                  style="width: 118px;">Nombre</th>
                              <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                  colspan="1" aria-label="Office: activate to sort column ascending"
                                  style="width: 51px;">Apellido Paterno</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Apellido Materno</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Seccion de comunidad</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Concepto</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Mes</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Año</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Fecha</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Monto</th>
                              <th class="sorting" tabindex="0" aria-controls="example"
                                  rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                                  style="width: 51px;">Cantidad</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr role="row" class="odd">
                              <td class="sorting_1"></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                              <th rowspan="1" colspan="1"></th>
                          
                            </tr>
                            </tfoot>
                          </table>
                          </div>
                        </div>
                        </div>
                        <br>
                
                          <div class="row">
                            <div class="col-sm-12 col-md-5">
                              <div class="form-group">
                                <p style="color:rgb(15, 15, 15)";>
                                <label for="inputText3" class="col-form-label" disabled="">Monto Total</label></p>
                                <input id="inputText3" type="text" class="form-control" disabled="">
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-6"><div class="dt-buttons">
                            <button class="btn btn-outline-rounted buttons-copy buttons-html5" tabindex="0" aria-controls="example" type="button">
                              <span>Copiar</span></button>
                            <button class="btn btn-outline-rounted buttons-excel buttons-html5" tabindex="0" aria-controls="example" type="button">
                              <span>Exportar a Excel</span></button>
                            <button class="btn btn-outline-rounted buttons-pdf buttons-html5" tabindex="0" aria-controls="example" type="button">
                              <span>Exportar a PDF</span></button>
                            <button class="btn btn-outline-rounted buttons-print" tabindex="0" aria-controls="example" type="button">
                              <span>Imprimir</span></button>
                            <!--<button class="btn btn-outline-rounted buttons-collection dropdown-toggle buttons-colvis" tabindex="0" aria-controls="example"
                                    type="button" aria-haspopup="true">
                              <span>Column visibility</span>
                            </button>-->
                          </div>
                          </div>
                        </div>
                        
                      </div>
                      </div>

                    </div>
                  </div>
                  
                </div>
              </div>
              <!-- ============================================================== -->
              <!-- end data table  -->
              <!-- ============================================================== -->
            </div>
          </div>
        </section><!-- End Team Section -->
        <br>
    
        
              <!-- ============================================================== -->
              <!-- end data table  -->
              <!-- ============================================================== -->
            </div>
          </div>
        </section><!-- End Team Section -->
    
      </main><!-- End #main -->

    <?php
} else {
    header("location: ../login.php");
}

include ('../partials/footer.php');
?>

      