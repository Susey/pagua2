<?php include ('../partials/head.php'); ?>

<?php

if (isset($_SESSION["usuario"])) {

?>
<body>
<?php include_once ('../partials/header.php');?>

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background: url(../assets/img/slide/tipos-de-agua.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <img src ="../assets/img/icons/pagualog.png" width="300", height="150"  >
                <h2 class="animated fadeInDown">Bienvenido a <span>PAGUA</span></h2>
                <p class="animated fadeInUp">Horario de aplicación de pagos y consultas de 9:00 a 17:30 hrs</p>
                <a href="" class="btn-get-started animated fadeInUp">Read More</a>
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background: url(../assets/img/slide/slide-2.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animated fadeInDown">Información <span>Relevante</span></h2>
                <p class="animated fadeInUp">Mes de mayo: la cuota actual es: $900
                  </p>
                <a href="" class="btn-get-started animated fadeInUp">Read More</a>
              </div>
            </div>
          </div>

        
        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Section ======= -->
    <section id="featured" class="featured">
      <div class="container">

        <div class="row">
          <div class="col-lg-4">
            <div class="icon-box">
              <i class="icofont-computer"></i>
              <h3><a href="">Nota 1</a></h3>
              <p>Texto relevante</p>
            </div>
          </div>
          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="icon-box">
              <i class="icofont-image"></i>
              <h3><a href="">Nota 2</a></h3>
              <p>Texto relevante</p>
            </div>
          </div>
          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="icon-box">
              <i class="icofont-tasks-alt"></i>
              <h3><a href="">Nota 3</a></h3>
              <p>Texto relevante</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Featured Section -->

  <?php
  } else {
      header("location: ../login.php");
  }

  include ('../partials/footer.php');
  ?>
