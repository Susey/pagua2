<?php include ('../partials/head.php'); ?>

<?php

if (isset($_SESSION["usuario"])) {

?>

<?php include_once ('../partials/header.php');?>

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Inicio</a></li>
          <li>Pagos</li>
        </ol>
        <h2>Pagos</h2>
      </div>
    </section><!-- End Breadcrumbs -->
<!-- ======= Consulta Selecction ======= -->
<section id="about" class="about">
  <div class="container">
    <div class="row">
      <!--div class="col-lg-6">
        <img src="assets/img/icons/pagos.png" class="img-fluid" alt="" >
      </div>-->
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
          <div class="card-body">
            <div class="form-group">
              <div style="color:black">
                <label>Fecha:<small class="text-muted"></small></label>
                  <input type="date" class="form-control date-inputmask" id="date-mask" im-insert="true">
              </div>
            </div>
                <div class="col-sm-12 col-md-6">
                  <div id="example_filter" class="dataTables_filter">
                    <p style="color:rgb(37, 35, 35)";>Folio:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></p>
                </div>
              </div>
              
        
          <div style="color:black">
            <div class="form-group">
         <fieldset>
          <legend>Contribuyente</legend>
          <div class="col-sm-4 col-lg-3 mb-3 mb-sm-0">
            <label>Folio<small class="text-muted"></small></label>
          <input id="inputText3" type="text" class="form-control" disabled="">
        <div class="form-group">
          <label>Nombre(s)<small class="text-muted"></small></label>
          <input id="inputText3" type="text" class="form-control" disabled="">
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label" disabled="">Apellido Paterno</label>
          <input id="inputText3" type="text" class="form-control" disabled="">
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label"disabled="">Apellido Materno</label>
          <input id="inputText3" type="text" class="form-control" disabled="">
        </div>
      </fieldset>
      <fieldset>
        <legend>Fecha de pago</legend>
        <div class="form-group">
          <label for="inputText3" class="col-form-label" >De:</label>
          <input type="date" id="date-mask" im-insert="true"></div>
        </div>
        <div class="form-group">
          <label for="inputText3" class="col-form-label" >A:</label>
          <input type="date" id="date-mask" im-insert="true"></div>
        </div>
        </fieldset>
        <fieldset>
          <legend>Registros</legend>
          <div class="row">
            <div class="table-responsive">
            <div class="col-sm-12"><table id="example" class="table table-striped table-bordered second dataTable" style="width: 100%;"
              role="grid" aria-describedby="example_info">
              <thead>
              <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example"
                    rowspan="1" colspan="1" aria-sort="ascending"
                    aria-label="Name: activate to sort column descending"
                    style="width: 73px;">Fecha</th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                    colspan="1" aria-label="Position: activate to sort column ascending"
                    style="width: 118px;">Descripción</th>
                <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                    colspan="1" aria-label="Office: activate to sort column ascending"
                    style="width: 51px;">Cantidad</th>
                <th class="sorting" tabindex="0" aria-controls="example"
                    rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                    style="width: 51px;">Precio unitario</th>
                <th class="sorting" tabindex="0" aria-controls="example"
                    rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending"
                    style="width: 51px;">Precio total</th>
              </tr>
              </thead>
              <tbody>
              <tr role="row" class="odd">
                <td class="sorting_1"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              </tbody>
              <tfoot>
              <tr>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1"></th>
              </tr>
              </tfoot>
            </table>
            </div>
          </div>
          </div>
        </fieldset>
        </div> 
        <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="form-group">
              <p style="color:rgb(116, 111, 111)";>
              <label for="inputText3" class="col-form-label" disabled="">Monto Total</label></p>
              <input id="inputText3" type="text" class="form-control" disabled="">
            </div>
          </div>
        </div>
        </div>
        <br>
        <a href="#" class="btn btn-rounded btn-success">Pagar</a>
        <a href="#" class="btn btn-rounded btn-danger">Cancelar</a>
      </div>
      </div>        
      </div>
           
    </div>
  </div>
</section>

    <?php
    } else {
        header("location: ../login.php");
    }

    include ('../partials/footer.php');
    ?>

    