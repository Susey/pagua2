<?php include ('../partials/head.php'); ?>

<?php

if (isset($_SESSION["usuario"])) {
# if ($_SESSION["usuario"]["idTipoUsuario"] == 2) {
#     header("location:usuario.php");
# }
?>

<?php include_once ('../partials/header.php');?>


    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Inicio</a></li>
          <li>Registro</li>
        </ol>
          <h2>Usuarios</h2>
      </div>
    </section><!-- End Breadcrumbs -->

  
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <img src="../assets/img/icons/adduser.png" class="img-fluid" alt="" >
          </div>
          <!--<div class="card-body border-top">-->
            <div class="card-body">
            <h4>Registrar Usuario</h4>
            <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Buscar usuario:<input type="search" 
            class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0"></label>
            <button class="btn btn-primary" type="submit">Buscar</button>
            <div class="form-group">
              <label for="inputText3" class="col-form-label">Nombre de usuario</label>
              <input id="inputText3" type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="inputPassword">Contraseña para el nuevo usuario</label>
              <input id="inputPassword" type="password" placeholder="Contraseña" class="form-control">
            </div>
            <div class="form-group">
              <label for="inputPassword">Confirmar Contraseña</label>
              <input id="inputPassword" type="password" placeholder="Contraseña" class="form-control">
            </div>
          </div>
            <!--<div class="card-body border-top">-->
              <div>
              <h4>Privilegios</h4>
              <form>
                <label class="custom-control custom-checkbox custom-control-inline">
                  <input type="checkbox" checked="" class="custom-control-input"><span class="custom-control-label">Administrador</span>
              </label>
              <label class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" checked="" class="custom-control-input"><span class="custom-control-label">Operador</span>
              </label>
              </form>
              <br>
              
              <a href="#" class="btn btn-rounded btn-success">Modificar</a>
              <a href="#" class="btn btn-rounded btn-danger">Cancelar</a>
          </div>
        </div>
        <br></br>
        <p style="text-align:center;">
          Los usuarios registrados en esta sección podrán iniciar sesión con su nombre de usuario y contraseña asignada por el 
          administrador y así mismo podrá acceder a los privilegios que se le asignen,
        </p>
      </div>
    </div>
    </section>
    <?php
} else {
    header("location: ../login.php");
}

include ('../partials/footer.php');
?>
  