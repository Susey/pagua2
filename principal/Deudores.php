<?php include ('../partials/head.php'); ?>

<?php

if (isset($_SESSION["usuario"])) {

    ?>

    <?php include_once ('../partials/header.php');?>


    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Inicio</a></li>
          <li>Reportes</li>
        </ol>
        <h2>Deudores</h2>
      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Contribuyentes Selecction ======= -->
 <section id="about" class="about">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="assets/img/icons/calendar.png" class="img-fluid" alt="" >
      </div>
      <div class="card-body border-top">
        <h4>Deudores</h4>
        <div class="input-group mb-3">
          <div class="input-group-prepend be-addon show">
              <button tabindex="-1" type="button" class="btn btn-secondary">Sección de la comunidad</button>
              <button tabindex="-1" data-toggle="dropdown" type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" 
              aria-expanded="true"><span class="sr-only">Toggle Dropdown</span></button>
              <div class="dropdown-menu should" x-placement="bottom-start" style="position: absolute; transform: translate3d(99px, 
              41px, 0px); top: 0px; left: 0px; will-change: transform;">
              <a href="#" class="dropdown-item">Arriba</a>
              <a href="#" class="dropdown-item">Abajo</a>
              <a href="#" class="dropdown-item">Centro</a>
                 <!--<div class="dropdown-divider"></div><a href="#" class="dropdown-item">Separated link</a>--> 
              </div>
          </div>
          <input type="text" class="form-control">
         
      </div>
      <button class="btn btn-primary" type="submit">Traer</button>
        <div class="form-group">
          <div class="card-body">
            <div class="table-responsive">
              <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row">
                <div class="col-sm-12 col-md-6">
                  <div id="example_filter" class="dataTables_filter">
                    <label>Buscar:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></label>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-sm-12"><table id="example" class="table table-striped table-bordered second dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                    <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 73px;">Identificado</th>
                      <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 118px;">Fecha</th>
                      <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 51px;">Monto</th>
                      <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 51px;">Saldov</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr role="row" class="odd">
                      <td class="sorting_1">Airi Satou</td>
                      <td>Accountant</td>
                      <td>Tokyo</td>
                      <td>33</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                      <th rowspan="1" colspan="1">Identificado</th>
                      <th rowspan="1" colspan="1">Fecha</th>
                      <th rowspan="1" colspan="1">Monto</th>
                      <th rowspan="1" colspan="1">Saldo</th>
                    </tr>
                    </tfoot>
                  </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Vista continuación</div>
                  </div>
                  <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                      <ul class="pagination">
                        <li class="paginate_button page-item previous disabled" id="example_previous">
                          <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Anterior</a>
                        </li>
                        <li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                        <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                        <li class="paginate_button page-item next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="7" tabindex="0" class="page-link">Siguiente</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-6"><div class="dt-buttons">
            <button class="btn btn-rounted- buttons-copy buttons-html5" tabindex="0" aria-controls="example" type="button">
              <span>Copy</span></button>
            <button class="btn btn-rounted-light buttons-excel buttons-html5" tabindex="0" aria-controls="example" type="button">
              <span>Excel</span></button>
            <button class="btn btn-rounted-light buttons-pdf buttons-html5" tabindex="0" aria-controls="example" type="button">
              <span>PDF</span></button>
            <button class="btn btn-rounted-light buttons-print" tabindex="0" aria-controls="example" type="button">
              
          </div>
          </div>
          <br>
          <!--<a href="#" class="btn btn-rounded btn-success">Guardar</a>
          <a href="#" class="btn btn-rounded btn-danger">Cancelar</a>
          <a href="#" class="btn btn-rounded btn-info">Modificar</a>
          <a href="#" class="btn btn-rounded btn-warning">Eliminar</a>-->
      </div>
    </div>
  </div>
</div>
 </section>
      <?php
     } else {
         header("location: ../login.php");
     }

     include ('../partials/footer.php');
     ?>
