<?php

class EntidadContribuyentes
{

    private $id;
    private $folio;
    private $nombre;
    private $apellidoPaterno;
    private $apellidoMaterno;
    private $fraccion;
    private $estado;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getFolio(){
        return $this->folio;
    }

    public function setFolio($folio){
        $this->folio = $folio;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getApellidoPaterno(){
        return $this->apellidoPaterno;
    }

    public function setApellidoPaterno($apellidoPaterno){
        $this->apellidoPaterno = $apellidoPaterno;
    }

    public function getApellidoMaterno(){
        return $this->apellidoMaterno;
    }

    public function setApellidoMaterno($apellidoMaterno){
        $this->apellidoMaterno = $apellidoMaterno;
    }

    public function getFraccion(){
        return $this->fraccion;
    }

    public function setFraccion($fraccion){
        $this->fraccion = $fraccion;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function setEstado($estado){
        $this->estado = $estado;
    }

}
