<?php
class ListaNegra
{
	private $idTicket;
	private $numeroTicket;
    private $folioContibuyente;
    private $nombreContribuyente;
    private $apellidoPaterno;
    private $apellidoMaterno;
    private $fraccion;
    private $concepto;
    private $mes;
    private $anio;
    private $fecha;
    private $monto;
    private $cantidad;

	public function __GET($nombre){ return $this->$nombre; }
	public function __SET($nombre, $valor){ return $this->$nombre = $valor; }
}
?>