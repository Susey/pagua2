<?php

include 'LogicaNegocio/ufinal.php';

session_start();

header('Content-type: application/json');
$resultado = array();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["txtUsuario"]) && isset($_POST["txtPassword"])) {

        $txtUsuario  = (new UsuarioControlador)->validar_campo($_POST["txtUsuario"]);
        $txtPassword = (new UsuarioControlador)->validar_campo($_POST["txtPassword"]);
        $resultado = array("estado" => "true");

        if (UsuarioControlador::login($txtUsuario, $txtPassword)) {
            $usuario             = (new UsuarioControlador)->getUsuario($txtUsuario, $txtPassword);
            $_SESSION["usuario"] = array(
                "id"         => $usuario->getId(),
                "usuario"    => $usuario->getUsuario(),
                "idTipoUsuario" => $usuario->getIdTipoUsuario(),
            );
            return print(json_encode($resultado));
        }
    }
}
$resultado = array("estado" => "false");
return print(json_encode($resultado));
